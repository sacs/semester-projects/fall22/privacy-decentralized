#!/bin/bash

decpy_path=/home/carevic/decentralizepy/eval
cd $decpy_path

env_python=~/mldev//bin/python3
graph=~/decentralizepy/run_configuration/36regular_4degree.edges
original_config=~/decentralizepy/run_configuration/gradient_inversion_config_cifar10_2_nodes.ini
config_file=/tmp/config.ini
procs_per_machine=18
machines=2
iterations=500
test_after=20
eval_file=testingPeerSampler.py
log_level=INFO

m=`cat $(grep addresses_filepath $original_config | awk '{print $3}') | grep $(/sbin/ifconfig enp3s0f0 | grep 'inet ' | awk '{print $2}') | cut -d'"' -f2`
echo M is $m
log_dir=$(date '+%Y-%m-%dT%H:%M')/machine$m
mkdir -p $log_dir

cp $original_config $config_file
# echo "alpha = 0.10" >> $config_file
$env_python $eval_file -ro 0 -tea $test_after -ld $log_dir -mid $m -ps $procs_per_machine -ms $machines -is $iterations -gf $graph -ta $test_after -cf $config_file -ll $log_level -ctr 0 -cte 0 -wsd $log_dir
