import torch
import numpy as np
from tqdm import tqdm
from scipy import special
from tensorflow.keras.backend import categorical_crossentropy as cce
from tensorflow.keras.backend import constant
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import AttackInputData
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import SlicingSpec
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import AttackType
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack import membership_inference_attack as mia
import inversefed
import matplotlib.pyplot as plt

def get_data(model, loader, num_classes = 10, device=torch.device('cpu')):
    """
    Get the data in the right format for the attacks module.
    
    Arguments:
        model: the targeted model that will be attacked.
        device: the device where the model is going to be run in ('cuda' or 'cpu').
        loader: the data loader from where the function converts the data. 

    Returns:
        np.array: logit representation of running model with input from loader.
        np.array: loss values of running model with input from loader.
        np.array: labels returned by running model with input from loader.
    """
    model.eval()
    logit, prob, label = [], [], []
    with torch.no_grad():
        for (data, target) in loader:
            data, target = data.to(device), target.to(device)
            logit.append(np.array(model(data)))
            prob.append(np.array(special.softmax(logit[-1], axis = 1)))
            label.append(np.array(target))
    logit = np.concatenate(tuple(logit), axis = 0)
    prob = np.concatenate(tuple(prob), axis = 0)
    label = np.concatenate(tuple(label), axis = 0)
    y = []
    for i in label:
        y.append(np.array([0]*i + [1] + [0]*(num_classes-1-i)))
    y = np.array(y)
    loss = np.array(cce(constant(y), constant(prob), from_logits = False))
    return logit, loss, label

def membership_inference_attack(model, train_loader, test_loader, enable_attacks = False, device = torch.device('cpu')):
    """
    Run a black-box membership inference attack in the model using the data provided.

    Arguments:
        model: the targeted model that will be attacked.
        device: the device where the model is going to be run in ('cuda' or 'cpu').
        train_loader: data loader with the data used to train the model.
        test_loader: data loader with datat that was ***not*** used to train the model.
        enable_attacks: enable two other kinds of attacks

    Returns:
        AttackResults: logit representation of running model with input from loader.
    """
    logits_train, loss_train, labels_train = get_data(model=model, device=device, loader=train_loader)
    logits_test, loss_test, labels_test = get_data(model=model, device=device, loader=test_loader)
    input = AttackInputData(
                logits_train = logits_train,
                logits_test = logits_test,
                loss_train = loss_train,
                loss_test = loss_test,
                labels_train = labels_train,
                labels_test = labels_test)
    attack_types = [AttackType.THRESHOLD_ATTACK]
    if enable_attacks:
        attack_types = attack_types + [AttackType.THRESHOLD_ATTACK]
    attacks_result = mia.run_attacks(input,
                                 SlicingSpec(
                                     entire_dataset = True,
                                     by_class = True,
                                     by_classification_correctness = True
                                 ),
                                 attack_types = attack_types)
    return attacks_result



def gradient_inversion_attack(model,input_gradients, image_shape, original_samples=None, original_labels=None, stats= None):
    '''
    Run the gradeint inversion attack.
    Source code could be found on: https://github.com/JonasGeiping/invertinggradients
    NOTE: WORKS ONLY WITH SGD WITHOUT MOMENTUM!

    Parameters:
    -----------
        model: PyTorch model
            The targetted model. Note that it should be the model from the previous iteration
        input_gradients: torch.tensor
              Gradients on the input batch
        original_samples: torch.tensor
             [Optional] The batch of samples
        image_shape: torch.shape
             Shape of the one sample (without batch dimension)
        original_labels: torch.tensor
             [Optional] The labels in the batch 

    Returns: tuple
    --------
        closest_sample: torch.Tensor
            The orignial image(s)
            In case where the labels are provided as well, we return whole batch

        output: torch.Tensor
            The reconstructed image(s)
            In case where the labels are provided as well, we reconstruct each sample in the batch

        statistics: dict
            If `orinigal_labels` is None: 
                    The reconstruction loss, mse (best-fitting orignal and reconstructed img) and psnr (best-fitting orignal vs reconstructed image)
            else:
                The reconstruction loss, mse  and psnr averaged across whole batch
            Note that this in case where `original_samples` is None, this is an empty dict
    '''


    # Instantiate config WORKS WELL ON FASHION MNIST DATASAET
    config = dict(signed=True,
                boxed=True,
                cost_fn='sim',
                indices='def',
                weights='equal',
                lr=0.1,
                optim='adam',
                restarts=1,
                max_iterations=500,
                total_variation=1e-6,
                init='randn',
                filter='median',
                lr_decay=True,
                scoring_choice='first')

    # Mean and std of datasets.
    # Should be normalized by default
    dm, ds = stats
    # Init reconstruction attack
    # Among the entire batch (i.e., mean of all gradients), we only aim to reconstruct the one (best-fitting) sample

    num_images = 1
    if (original_labels is not None):
        num_images = len(original_labels)
    rec_machine = inversefed.GradientReconstructor(model, (dm, ds), config, num_images=num_images)

    # Run the gradient inversion attack
    # The attack is beeter if we provide the labels as well.
    output, stats = rec_machine.reconstruct(
        input_data=input_gradients, 
        labels=original_labels,
        img_shape=image_shape)
    
    # These are stats from original attack 
    stats = {"inversefed_stats": stats}
    closest_sample = original_samples
    if original_samples is not None:
        mse = None
        psnr = None

        if original_labels is None:
            # Obtain the best-fit
            best_ind = torch.argmin((original_samples - output.detach()).pow(2).flatten(1).mean(axis=1))
            best = original_samples[best_ind].unsqueeze(0)
            closest_sample = best

            # Calculate stats
            psnr = inversefed.metrics.psnr(output, best, factor=1/ds)
            mse = (best - output).pow(2).mean()

        else:

            # Calculate stats
            psnr = inversefed.metrics.psnr(output, original_samples, factor=1/ds)
            mse = (original_samples - output).pow(2).mean()

        # Update dict
        stats['mse'] = mse
        stats['psnr'] = psnr
      
    return closest_sample, output, stats

    
def plot_original_reconstructed_gradient_inversion(list_tensors, figtitle,subplot_titles,mean= None, std= None, filename=None):
    '''
    This is a helper function to plot the original and reconstructed samples

    Parameters: 
    -----------
        list_tensors: List of batches of images (list contains original and reconstructed batches of samples)
        figtitle: str 
            Title of a figure
        subplot_titles: list[str]
            Titles of subplots
        mean: torch.Tensor
            training dataset mean
        std: torch.Tensor
            training dataset std
    
    Returns:
    --------
        None
    '''

    # Need to have original and reconstructed image
    assert len(list_tensors) ==2

    fig = plt.figure(constrained_layout=True, figsize=(list_tensors[0].shape[0]*4, len(list_tensors)*4))
    fig.suptitle(figtitle)

    # create 3x1 subfigs
    subfigs = fig.subfigures(nrows=len(list_tensors), ncols=1)

    for row, subfig in enumerate(subfigs):
        subfig.suptitle(subplot_titles[row])

        # create 1x3 subplots per subfig
        axes = subfig.subplots(nrows=1, ncols=list_tensors[row].shape[0])
        tensor = list_tensors[row]
        tensor = tensor.clone().detach()
        if mean is not None:
            tensor.mul_(std).add_(mean).clamp_(0, 1)
        if tensor.shape[0] == 1:
            axes.imshow(tensor[0].permute(1, 2, 0).cpu())
        else:
            for i, im in enumerate(tensor):
                axes[i].imshow(im.permute(1, 2, 0).cpu())
    if filename is not None: 
        plt.savefig(filename)
        print("Saved reconstructed image to file: ", filename)
# plot_original_reconstructed(
#     list_tensors=[best, output],
#     figtitle="Original vs. Reconstructed imgs",
#     subplot_titles=["Original image",  f"Reconstructed image [Rec. loss: {stats['opt']:2.4f} | MSE: {test_mse:2.4f} "
#         f"| PSNR: {test_psnr:4.2f}"])