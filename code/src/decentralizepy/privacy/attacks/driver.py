# from sharing import Sharing
from .passive_attacks import membership_inference_attack, gradient_inversion_attack, plot_original_reconstructed_gradient_inversion
from torch.utils.data import DataLoader, random_split
import tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.plotting as plotting
import torch
import os
import pandas as pd
import torch
import copy
'''
DRIVER FILE TO MOUNT ATTACKS

Add to config file (SHARING section) paramters:

mia_attack = False                                      // Enable/disable mia attacks
gradient_inversion_attack = True                        // Enable/disable GI attacks
lr = 0.02                                               // Needed for gradient approx in GI attack
dataset_stats = [[0.5,0.5,0.5], [0.5,0.5,0.5]]          // Needed got GI attak, dataset mean and std. MUST BE A LIST OF LISTS!
frequency_of_attack = 20                                // after how many iterations for perform the attack
'''

PRESTEP = 'PRE-STEP'
POSTSTEP = 'POST-STEP'
NEIGHBOUR = 'NEIGHBOUR'
ATTACKS_DATASET_DIR = './privacy_datasets'
TRAINING_DATASET = "dataloader_train_set_m{}_r{}.pkl"

def mount_attack(share, location_of_attack):
    '''
    Driver function.
    Mounted in `_post_step()` function of `sharing.py`
    '''

    # Increment iteration number
    if(location_of_attack == PRESTEP):
        share.iteration_number += 1

    
    # MOUNT MEMBERSHIP INFERENCE ATTACK
    train_dataloader = share.dataset.get_trainset()
    length_tr_dataset = len(train_dataloader.dataset)

    # Align number of samples in train and test datasets CIFAR 10
    test_dataloader = share.dataset.get_testset()
    test_dataset =test_dataloader.dataset
    test_dataset = random_split(test_dataset, [length_tr_dataset, len(test_dataset)-length_tr_dataset])[0]
    test_dataloader = DataLoader(test_dataset, batch_size=test_dataloader.batch_size, shuffle=False)

    results_mia = membership_inference_attack(
        model= copy.deepcopy(share.model),
        device= torch.device('cuda' if torch.cuda.is_available() else 'cpu'),
        train_loader= train_dataloader,
        test_loader= test_dataloader
    )
 
    # Plot
    # plot_dir = os.path.join(share.log_dir, "privacy","plots", str(share.uid))
    # if not os.path.exists(plot_dir):
    #     os.makedirs(plot_dir)
    # plot_file_name = os.path.join(plot_dir,'privacy-plot-iter-{iteration_number}.jpg'.format(
    #         iteration_number=share.iteration_number))
    
    # print("Privacy plot written to file: " + str(plot_file_name))
    # plot_best_attack(
    #     attack_results=results_mia,
    #     file_name=plot_file_name)

    # Write summary
    df_results_mia = results_mia.calculate_pd_dataframe()
    df_results_mia['iteration'] = share.iteration_number
    df_results_mia['location_of_attack'] = location_of_attack
    summary_dir = os.path.join(share.log_dir, "privacy","summary", str(share.uid))
    if not os.path.exists(summary_dir):
        os.makedirs(summary_dir)

  
    summary_file_name = os.path.join(summary_dir,'privacy-summary-{location_of_attack}.json'.format(location_of_attack=location_of_attack))
    if os.path.exists(summary_file_name):
        df = pd.read_json(summary_file_name)
        df_results_mia = pd.concat([df,df_results_mia])
    df_results_mia.to_json(summary_file_name, orient='records')

    print("[{location_of_attack}] Privacy summary for iteration {iteration} written to file: {summary_file_name})".format(
            location_of_attack=location_of_attack,
            iteration=share.iteration_number,
            summary_file_name=summary_file_name)
        )
    

    
def mount_attack_on_neighbour(share, model_weights, location_of_attack, iteration, machine_id, rank):
    '''
    Driver function.
    Mounted in `_averaging()` function of `sharing.py`
    '''

    # Load model weights
    share.attack_model.load_state_dict(model_weights)
    
    # MOUNT MEMBERSHIP INFERENCE ATTACK
    dataset_path = os.path.join(ATTACKS_DATASET_DIR, TRAINING_DATASET.format(machine_id, rank))
    train_dataloader = torch.load(dataset_path)
    length_tr_dataset = len(train_dataloader.dataset)

    # Align number of samples in train and test datasets CIFAR 10
    test_dataloader = share.dataset.get_testset()
    test_dataset =test_dataloader.dataset
    test_dataset = random_split(test_dataset, [length_tr_dataset, len(test_dataset)-length_tr_dataset])[0]
    test_dataloader = DataLoader(test_dataset, batch_size=test_dataloader.batch_size, shuffle=False)

    results_mia = membership_inference_attack(
        model= share.attack_model,
        device= torch.device('cuda' if torch.cuda.is_available() else 'cpu'),
        train_loader= train_dataloader,
        test_loader= test_dataloader
    )


    # Write summary
    df_results_mia = results_mia.calculate_pd_dataframe()
    df_results_mia['iteration'] = iteration
    df_results_mia['location_of_attack'] = location_of_attack
    summary_dir = os.path.join(share.log_dir, "privacy","summary", str(share.uid))
    if not os.path.exists(summary_dir):
        os.makedirs(summary_dir)

  
    summary_file_name = os.path.join(summary_dir,'privacy-summary-{location_of_attack}.json'.format(location_of_attack=location_of_attack))
    if os.path.exists(summary_file_name):
        df = pd.read_json(summary_file_name)
        df_results_mia = pd.concat([df,df_results_mia])
    df_results_mia.to_json(summary_file_name, orient='records')

    print("[{location_of_attack}] Privacy summary for iteration {iteration} written to file: {summary_file_name})".format(
            location_of_attack=location_of_attack,
            iteration=iteration,
            summary_file_name=summary_file_name)
        )
    
    
  


def plot_best_attack(attack_results, file_name):
    
    # Get the best attack
    max_auc_attacker = attack_results.get_result_with_max_auc()
    max_advantage_attacker = attack_results.get_result_with_max_attacker_advantage()

    # Plot
    figure  = plotting.plot_roc_curve(max_auc_attacker.roc_curve)
    figure.savefig(file_name)



  
def mount_gradient_inversion_attack_on_neighbour(share, model_weights, iteration, location_of_attack, machine_id, rank, original_batch, dataset_stats):
    '''
    Driver function for gradient inversion attack.
    Mounted in `_averaging()` function of `sharing.py`

    Parameters
    ----------
        dataset_stats: Tuple
        mean and std of the dataset. Provide this in the sharing section of config file.
    '''

    # If iteration is less than 2, just save the model weights and return
    if iteration < 2:
        share.GI_attack_model_neighbour.load_state_dict(model_weights)
        return

    # Load model weights
    old_model = copy.deepcopy(share.GI_attack_model_neighbour) 
    share.GI_attack_model_neighbour.load_state_dict(model_weights)
    
    # Approximate gradients
    reconstructed_gradients_neighbour = []
    for param_old, param_new in zip(old_model.parameters(), share.GI_attack_model_neighbour.parameters()):
        param_new.requires_grad = True
        param_old.requires_grad = True
        reconstructed_gradients_neighbour.append((param_old - param_new) / share.lr)

    reconstructed_gradients_poststep = []
    for param_old, param_new in zip(share.GI_attack_model_poststep.parameters(), share.GI_attack_model_neighbour.parameters()):
        param_new.requires_grad = True
        param_old.requires_grad = True
        reconstructed_gradients_poststep.append((param_old - param_new) / share.lr)
   
    # Extract samples
    original_samples, orignial_labels = original_batch

    # Run attack on models btw communications
    original_image_neighbour, reconstructed_image_neighour, stats_neighbour = gradient_inversion_attack(
        model=old_model,
        input_gradients=reconstructed_gradients_neighbour,
        image_shape=original_samples[0].shape,
        original_labels=orignial_labels,
        original_samples=original_samples,
        stats=dataset_stats
    )


    # Run attack on models btw aggregation and comm
    original_image_poststep, reconstructed_image_poststep, stats_poststep  = gradient_inversion_attack(
        model=share.GI_attack_model_poststep,
        input_gradients=reconstructed_gradients_poststep,
        image_shape=original_samples[0].shape,
        original_labels=orignial_labels,
        original_samples=original_samples,
        stats=dataset_stats
    )

    print("POSTSTEP:", stats_poststep, "    NEIGHOUR:", stats_neighbour)

    # Write summary
    summary_dir = os.path.join(share.log_dir, "privacy","summary", str(share.uid))
    if not os.path.exists(summary_dir):
        os.makedirs(summary_dir)


    locations_of_attack = ["POSTSTEP-"+location_of_attack, "COMMUNICATION-"+ location_of_attack]
    df_results = None
    original_images = [original_image_poststep, original_image_neighbour]
    reconstructed_images = [reconstructed_image_poststep, reconstructed_image_neighour]
    for i, (location, stats) in enumerate(zip(locations_of_attack, [stats_poststep, stats_neighbour])):
        tmp = pd.DataFrame({})
        tmp['iteration'] = [iteration]
        tmp['psnr'] = stats['psnr']
        tmp['mse'] = stats['mse']
        tmp['inversefed_stats'] = [stats['inversefed_stats']]
        tmp['location_of_attack'] = location
        if df_results is None:
            df_results = tmp
        else:
            df_results = pd.concat([df_results, tmp])
        
        if stats["mse"] < 0.15:
            plot_file_name = os.path.join(summary_dir,'gradient_inversion-{location_of_attack}.jpg'.format(location_of_attack=location))
            if not os.path.exists(plot_file_name):
                plot_original_reconstructed_gradient_inversion(
                    [original_images[i], reconstructed_images[i]],
                    figtitle="Original vs. Reconstructed image for the iteration: {}".format(iteration),
                    subplot_titles= ["Original image",  f"Reconstructed image [Rec. loss: {stats['inversefed_stats']['opt']:2.4f} | MSE: {stats['mse']:2.4f} "
                f"| PSNR: {stats['psnr']:4.2f}"], 
                    filename=plot_file_name
                )

    summary_file_name = os.path.join(summary_dir,'gradient_inversion-summary-{location_of_attack}.json'.format(location_of_attack=location_of_attack))
    if os.path.exists(summary_file_name):
        df = pd.read_json(summary_file_name)
        df_results = pd.concat([df,df_results])
    df_results.to_json(summary_file_name, orient='records', default_handler=str)

    print("[{location_of_attack}] GI summary for iteration {iteration} written to file: {summary_file_name})".format(
            location_of_attack=location_of_attack,
            iteration=iteration,
            summary_file_name=summary_file_name)
        )

