import logging
import torch
import torch.nn.functional as F
import torchvision
import torchvision.transforms as transforms
from torch import nn
from torch.utils.data import DataLoader
from decentralizepy.datasets.Dataset import Dataset
from decentralizepy.datasets.Partitioner import DataPartitioner, KShardDataPartitioner
from decentralizepy.mappings.Mapping import Mapping
from decentralizepy.models.Model import Model
import os
from decentralizepy.datasets.CIFAR10 import CIFAR10, LeNet, CNN 
NUM_CLASSES = 10
from decentralizepy.privacy.attacks.driver import TRAINING_DATASET, ATTACKS_DATASET_DIR
class DPSGD_CIFAR10(CIFAR10):
    """
    Class for the differentially privacte CIFAR10- dataset

    """

    def __init__(
        self,
        rank: int,
        machine_id: int,
        mapping: Mapping,
        n_procs="",
        train_dir="",
        test_dir="",
        sizes="",
        test_batch_size=1024,
        partition_niid=False,
        shards=1,
    ):
        """
        Constructor which reads the data files, instantiates and partitions the dataset

        Parameters
        ----------
        rank : int
            Rank of the current process (to get the partition).
        machine_id : int
            Machine ID
        mapping : decentralizepy.mappings.Mapping
            Mapping to convert rank, machine_id -> uid for data partitioning
            It also provides the total number of global processes
        train_dir : str, optional
            Path to the training data files. Required to instantiate the training set
            The training set is partitioned according to the number of global processes and sizes
        test_dir : str. optional
            Path to the testing data files Required to instantiate the testing set
        sizes : list(int), optional
            A list of fractions specifying how much data to alot each process. Sum of fractions should be 1.0
            By default, each process gets an equal amount.
        test_batch_size : int, optional
            Batch size during testing. Default value is 64
        partition_niid: bool, optional
            When True, partitions dataset in a non-iid way

        """
        self.dp_dataloader = None
        super().__init__(
            rank=rank,
            machine_id=machine_id,
            mapping=mapping,
            n_procs=n_procs,
            train_dir=train_dir,
            test_dir=test_dir,
            sizes=sizes,
            test_batch_size=test_batch_size,
            partition_niid=partition_niid,
            shards=shards
        )



    def get_trainset(self, batch_size=1, shuffle=False):
        """
        Function to get the training set

        Parameters
        ----------
        batch_size : int, optional
            Batch size for learning

        Returns
        -------
        torch.utils.Dataset(decentralizepy.datasets.Data)

        Raises
        ------
        RuntimeError
            If the training set was not initialized

        """
        if self.dp_dataloader is not None:
            return self.dp_dataloader
        else:
            return super().get_trainset(batch_size, shuffle)


    def set_dp_dataloder(self, dp_dataloader):
        self.dp_dataloader = dp_dataloader


