import logging
import torch
from decentralizepy.training.Training import Training
import json


class LabelFlipping(Training):
    """
    This class implements the training module for a single node with label flipping attack.
    NOTE: Implements targeted label flipping attack. Source paper : https://arxiv.org/pdf/2007.08432.pdf 

    Usage:
        In the config file, section for tranining parameters add:
    
    [TRAIN_PARAMS]
    training_package = decentralizepy.privacy.training.LabelFlipping
    training_class = LabelFlipping
    label_attack_substitution_map = {"0" : 4, "5":6}  //  every occurence of label 0 is going to be changed with the label 4, every occurence of label 5 is going to be changed with label 6 etc.
    label_attack_adversaries = [0, 12, 15, 18] // Arbitrary, the list of UIDs of adversary nodes



    """

    def __init__(
        self,
        rank,
        machine_id,
        mapping,
        model,
        optimizer,
        loss,
        log_dir,
        rounds="",
        full_epochs="",
        batch_size="",
        shuffle="",
        label_attack_substitution_map = None,
        label_attack_adversaries = None,
        **kwargs
    ):
        """
        Constructor

        Parameters
        ----------
        rank : int
            Rank of process local to the machine
        machine_id : int
            Machine ID on which the process in running
        mapping : decentralizepy.mappings
            The object containing the mapping rank <--> uid
        model : torch.nn.Module
            Neural Network for training
        optimizer : torch.optim
            Optimizer to learn parameters
        loss : function
            Loss function
        log_dir : str
            Directory to log the model change.
        rounds : int, optional
            Number of steps/epochs per training call
        full_epochs : bool, optional
            True if 1 round = 1 epoch. False if 1 round = 1 minibatch
        batch_size : int, optional
            Number of items to learn over, in one batch
        shuffle : bool
            True if the dataset should be shuffled before training
        label_attack_substitution_map:
            Map for substitution of source class (Csrc) with targeted class (Ctarget)
        adversaries:
            List of UIDs of malicious nodes
        """
        super().__init__(
        rank=rank,
        machine_id= machine_id,
        mapping=mapping,
        model=model,
        optimizer=optimizer,
        loss=loss,
        log_dir=log_dir,
        rounds=rounds,
        full_epochs=full_epochs,
        batch_size=batch_size,
        shuffle=shuffle
        )

        # Init fields
        self.label_attack_substitution_map = json.loads(label_attack_substitution_map)
        self.adversaries = json.loads(label_attack_adversaries)
        self.uid = self.mapping.get_uid(self.rank, self.machine_id)

        
    

    def trainstep(self, data, target):
        """
        One training step on a minibatch.

        Parameters
        ----------
        data : any
            Data item
        target : any
            Label

        Returns
        -------
        int
            Loss Value for the step

        """

        # TODO: CHANGE THIS TO BE MORE EFFICIENT!
        if self.uid in self.adversaries:
            for class_src, class_target in self.label_attack_substitution_map.items():
                class_src = int(class_src)
                target[target == class_src] = class_target
        self.optimizer.zero_grad()
        output = self.model(data)
        loss_val = self.loss(output, target)
        loss_val.backward()
        self.optimizer.step()
        return loss_val.item()
