import logging
import torch
import opacus
from decentralizepy.training.Training import Training


class DPSGD(Training):
    """
    This class implements the training module for a single node. Training is in differential privacy manner.
    NOTE: In order to run this trainig use opacus 1.2.0
            pip install opacus==1.2.0
            CURRENTLY SUPPORTED ONLY WITH CLASSES: DPSGD_Node and its child classes.
    In config file, add following:
    training_package = decentralizepy.privacy.training.DPSGD
    training_class = DPSGD
    dp = True
    noise_multiplier = 1.1
    max_grad_norm = 1.0

    """

    def __init__(
        self,
        rank,
        machine_id,
        mapping,
        model,
        optimizer,
        loss,
        log_dir,
        rounds="",
        full_epochs="",
        batch_size="",
        shuffle="",
        **kwargs
    ):
        """
        Constructor

        Parameters
        ----------
        rank : int
            Rank of process local to the machine
        machine_id : int
            Machine ID on which the process in running
        mapping : decentralizepy.mappings
            The object containing the mapping rank <--> uid
        model : torch.nn.Module
            Neural Network for training
        optimizer : torch.optim
            Optimizer to learn parameters
        loss : function
            Loss function
        log_dir : str
            Directory to log the model change.
        rounds : int, optional
            Number of steps/epochs per training call
        full_epochs : bool, optional
            True if 1 round = 1 epoch. False if 1 round = 1 minibatch
        batch_size : int, optional
            Number of items to learn over, in one batch
        shuffle : bool
            True if the dataset should be shuffled before training.

        """
        super().__init__(
        rank=rank,
        machine_id= machine_id,
        mapping=mapping,
        model=model,
        optimizer=optimizer,
        loss=loss,
        log_dir=log_dir,
        rounds=rounds,
        full_epochs=full_epochs,
        batch_size=batch_size,
        shuffle=shuffle
        )