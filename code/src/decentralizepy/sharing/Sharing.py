import importlib
import logging
import copy
import pandas as pd
import os
import json
from platform import machine
from decentralizepy.privacy.attacks.driver import mount_attack, POSTSTEP, PRESTEP, NEIGHBOUR, mount_attack_on_neighbour, mount_gradient_inversion_attack_on_neighbour

import torch


class Sharing:
    """
    API defining who to share with and what, and what to do on receiving

    """

    def __init__(
        self,
        rank,
        machine_id,
        communication,
        mapping,
        graph,
        model,
        dataset,
        log_dir,
        compress=False,
        compression_package=None,
        compression_class=None,
        lr=None,
        mia_attack=False,
        gradient_inversion_attack = False,
        dataset_stats = None,
        frequency_of_attack = 1
    ):
        """
        Constructor

        Parameters
        ----------
        rank : int
            Local rank
        machine_id : int
            Global machine id
        communication : decentralizepy.communication.Communication
            Communication module used to send and receive messages
        mapping : decentralizepy.mappings.Mapping
            Mapping (rank, machine_id) -> uid
        graph : decentralizepy.graphs.Graph
            Graph reprensenting neighbors
        model : decentralizepy.models.Model
            Model to train
        dataset : decentralizepy.datasets.Dataset
            Dataset for sharing data. Not implemented yet! TODO
        log_dir : str
            Location to write shared_params (only writing for 2 procs per machine)

        """
        self.rank = rank
        self.machine_id = machine_id
        self.uid = mapping.get_uid(rank, machine_id)
        self.communication = communication
        self.mapping = mapping
        self.graph = graph
        self.model = model

        self.frequency_of_attack = frequency_of_attack
        self.mia_attack = mia_attack
        self.attack_model = copy.deepcopy(model)
        self.gradient_inversion_attack = gradient_inversion_attack
        if (gradient_inversion_attack):
            self.GI_attack_model_neighbour = copy.deepcopy(model)
            self.GI_attack_model_poststep = copy.deepcopy(model)
            self.lr=lr
            dataset_stats = json.loads(dataset_stats)
            self.dataset_stats = (torch.as_tensor(dataset_stats[0]),torch.as_tensor(dataset_stats[1]))
            if len(dataset_stats[0])==3:
                self.dataset_stats = (self.dataset_stats[0][:,None, None], self.dataset_stats[1][:,None, None])


        self.dataset = dataset
        self.communication_round = 0
        self.log_dir = log_dir
        self.iteration_number = 0
   

        self.shapes = []
        self.lens = []
        with torch.no_grad():
            for _, v in self.model.state_dict().items():
                self.shapes.append(v.shape)
                t = v.flatten().numpy()
                self.lens.append(t.shape[0])

        self.compress = compress

        if compression_package and compression_class:
            compressor_module = importlib.import_module(compression_package)
            compressor_class = getattr(compressor_module, compression_class)
            self.compressor = compressor_class()
            logging.info(f"Using the {compressor_class} to compress the data")
        else:
            assert not self.compress

    def compress_data(self, data):
        result = dict(data)
        if self.compress:
            if "params" in result:
                result["params"] = self.compressor.compress_float(
                    result["params"])
        return result

    def decompress_data(self, data):
        if self.compress:
            if "params" in data:
                data["params"] = self.compressor.decompress_float(
                    data["params"])
        return data

    def serialized_model(self):
        """
        Convert model to a dictionary. Here we can choose how much to share

        Returns
        -------
        dict
            Model converted to dict

        """
        to_cat = []
        with torch.no_grad():
            for _, v in self.model.state_dict().items():
                t = v.flatten()
                to_cat.append(t)
        flat = torch.cat(to_cat)
        data = dict()
        data["params"] = flat.numpy()
        return self.compress_data(data)

    def deserialized_model(self, m):
        """
        Convert received dict to state_dict.

        Parameters
        ----------
        m : dict
            received dict

        Returns
        -------
        state_dict
            state_dict of received

        """
        state_dict = dict()
        m = self.decompress_data(m)
        T = m["params"]
        start_index = 0
        for i, key in enumerate(self.model.state_dict()):
            end_index = start_index + self.lens[i]
            state_dict[key] = torch.from_numpy(
                T[start_index:end_index].reshape(self.shapes[i])
            )
            start_index = end_index

        return state_dict

    def _pre_step(self):
        """
        Called at the beginning of step.

        """

        if (self.mia_attack):
            if (self.iteration_number % self.frequency_of_attack==0):
                mount_attack(self, PRESTEP)
                # if (self.rank == 0 or self.rank == 8):
                #     mount_attack(self, PRESTEP)

    def _post_step(self):
        """
        Called at the end of step.

        """
        if (self.mia_attack):
            if (self.iteration_number % self.frequency_of_attack==0):
                mount_attack(self, POSTSTEP)

                # if (self.rank == 0 or self.rank == 8):
                #     mount_attack(self, POSTSTEP)

        if (self.gradient_inversion_attack):
            self.GI_attack_model_poststep.load_state_dict(self.model.state_dict())

    def _averaging(self, peer_deques):
        """
        Averages the received model with the local model

        """
        ## Added for privacy 
        models = []
        with torch.no_grad():
            total = dict()
            weight_total = 0
            for i, n in enumerate(peer_deques):
                data = peer_deques[n].popleft()
                degree, iteration = data["degree"], data["iteration"]
                ## Added for privacy attack
                n_machine_id, n_rank = data['machine_id'], data['rank']
                original_batch = data['original_batch']

                del data['original_batch']
                models.append(copy.deepcopy(data))
                del data['machine_id']
                del data['rank']
                del data["degree"]
                del data["iteration"]
                del data["CHANNEL"]

                logging.debug(
                    "Averaging model from neighbor {} of iteration {}".format(
                        n, iteration
                    )
                )
                data = self.deserialized_model(data)

                # Mount privacy attack
                # Always attack node 0 from node 1
                if (self.iteration_number % self.frequency_of_attack == 0):
                    if (self.rank==1 and n_machine_id == self.machine_id and n_rank == 0): 
                        # Mount membership inference attack
                        if(self.mia_attack):
                            mount_attack_on_neighbour(
                                location_of_attack= "{}-M{}-R{}".format(NEIGHBOUR, n_machine_id, n_rank),
                                model_weights=data,
                                iteration=iteration,
                                share=self,
                                machine_id = n_machine_id,
                                rank = n_rank
                            )
                        if self.gradient_inversion_attack:
                            # Mount gradient inversion attack
                            mount_gradient_inversion_attack_on_neighbour(
                                share=self,
                                iteration=iteration,
                                location_of_attack="{}-M{}-R{}".format(NEIGHBOUR, n_machine_id, n_rank),
                                machine_id=n_machine_id,
                                rank=n_rank,
                                model_weights=data,
                                original_batch=original_batch,
                                dataset_stats=self.dataset_stats
                            )


                # Metro-Hastings
                weight = 1 / (max(len(peer_deques), degree) + 1)
                weight_total += weight
                for key, value in data.items():
                    if key in total:
                        total[key] += value * weight
                    else:
                        total[key] = value * weight

            for key, value in self.model.state_dict().items():
                total[key] += (1 - weight_total) * value  # Metro-Hastings

        self.model.load_state_dict(total)
        # self.calculate_model_distances(
        #     models=models,
        #     location='PRESTEP'
        # )
        self._post_step()
        self.communication_round += 1

    def get_data_to_send(self):
        self._pre_step()
        data = self.serialized_model()
        my_uid = self.mapping.get_uid(self.rank, self.machine_id)
        all_neighbors = self.graph.neighbors(my_uid)
        data["degree"] = len(all_neighbors)
        data["iteration"] = self.communication_round

        ## Added for privacy attack
        data["machine_id"] =  self.machine_id
        data["rank"]= self.rank
        return data

    def _averaging_server(self, peer_deques):
        """
        Averages the received models of all working nodes

        """
        with torch.no_grad():
            total = dict()
            for i, n in enumerate(peer_deques):
                data = peer_deques[n].popleft()
                degree, iteration = data["degree"], data["iteration"]
                del data["degree"]
                del data["iteration"]
                del data["CHANNEL"]

                 ## Added for privacy attack
                n_machine_id, n_rank = data['machine_id'], data['rank']
                original_batch = data['original_batch']
                del data['machine_id']
                del data['rank']
                del data['original_batch']

                logging.debug(
                    "Averaging model from neighbor {} of iteration {}".format(
                        n, iteration
                    )
                )
                data = self.deserialized_model(data)
                weight = 1 / len(peer_deques)
                for key, value in data.items():
                    if key in total:
                        total[key] += weight * value
                    else:
                        total[key] = weight * value

        self.model.load_state_dict(total)
        self._post_step()
        self.communication_round += 1
        return total

    def calculate_model_distances(self, models, location):
        with torch.no_grad():
            tmp = pd.DataFrame()
            tmp['location'] = [location]
            tmp['self_node_id'] = [self.uid]
            distances = {}
            iteration = -1
            for data in models:
                iteration = data["iteration"]
                ## Added for privacy attack
                n_machine_id, n_rank = data['machine_id'], data['rank']
                del data['machine_id']
                del data['rank']
                del data["degree"]
                del data["iteration"]
                del data["CHANNEL"]

                data = self.deserialized_model(data)
                distance = 0
                for key, value in self.model.state_dict().items():
                    distance += ((value - data[key]) **2).sum()
                distance = distance.sqrt().item()
                distances[self.mapping.get_uid(n_rank, n_machine_id)] = distance
            tmp['iteration'] = [iteration]
            tmp['distances'] = [json.dumps(distances)]
                    
        # Save to file
        summary_dir = os.path.join(self.log_dir, "privacy","summary", str(self.uid))
        if not os.path.exists(summary_dir):
            os.makedirs(summary_dir)
        summary_file_name = os.path.join(summary_dir,'model_distances-{location}.json'.format(location=location))
        if os.path.exists(summary_file_name):
            df = pd.read_json(summary_file_name)
            tmp = pd.concat([df,tmp])
        tmp.to_json(summary_file_name, orient='records', default_handler=str)
        print("[{location_of_attack}] Model distances summary for iteration {iteration} written to file: {summary_file_name})".format(
                location_of_attack=location,
                iteration=iteration,
                summary_file_name=summary_file_name)
            )
