import logging

import numpy as np
import torch
import copy
from nngeometry.metrics import FIM
from nngeometry.object import PMatDiag, PMatKFAC, PMatEKFAC

from decentralizepy.sharing.PartialModel import PartialModel
import importlib

class FisherSharing(PartialModel):
    """
    This class implements partial model sharing with some random additions.

    """

    def __init__(
        self,
        rank,
        machine_id,
        communication,
        mapping,
        graph,
        model,
        dataset,
        log_dir,
        alpha=1.0,
        dict_ordered=True,
        save_shared=False,
        metadata_cap=1.0,
        compress=False,
        compression_package=None,
        compression_class=None,
        base_class=None,
        base_package=None,
        start_round=0,
        frequency=1,
        number_of_classes=10,
        subset_size=None,
        **kwargs
    ):
        """
        Constructor

        Parameters
        ----------
        rank : int
            Local rank
        machine_id : int
            Global machine id
        communication : decentralizepy.communication.Communication
            Communication module used to send and receive messages
        mapping : decentralizepy.mappings.Mapping
            Mapping (rank, machine_id) -> uid
        graph : decentralizepy.graphs.Graph
            Graph reprensenting neighbors
        model : decentralizepy.models.Model
            Model to train
        dataset : decentralizepy.datasets.Dataset
            Dataset for sharing data. Not implemented yet! TODO
        log_dir : str
            Location to write shared_params (only writing for 2 procs per machine)
        alpha : float
            Percentage of model to share
        dict_ordered : bool
            Specifies if the python dict maintains the order of insertion
        save_shared : bool
            Specifies if the indices of shared parameters should be logged
        metadata_cap : float
            Share full model when self.alpha > metadata_cap

        """
        super().__init__(
            rank=rank,
            machine_id=machine_id,
            communication=communication,
            mapping=mapping,
            graph=graph,
            model=model,
            dataset=dataset,
            log_dir=log_dir,
            alpha=alpha,
            dict_ordered=dict_ordered,
            save_shared=save_shared,
            metadata_cap=metadata_cap,
            compress=compress,
            compression_package=compression_package,
            compression_class=compression_class
        )

        self.start_round = start_round
        self.frequency = frequency
        self.subset_size = subset_size
        self.alpha = alpha
        self.dataset = dataset
        self.indices = None
        self.number_of_classes = number_of_classes

        self.base = None
        if base_package is not None and base_class is not None:
            base_module = importlib.import_module(base_package)
            base_class = getattr(base_module, base_class)
            self.base = base_class(
            rank=rank,
            machine_id=machine_id,
            communication=communication,
            mapping=mapping,
            graph=graph,
            model=model,
            dataset=dataset,
            log_dir=log_dir,
            alpha=alpha,
            dict_ordered=dict_ordered,
            save_shared=save_shared,
            metadata_cap=metadata_cap,
            compress=compress,
            compression_package=compression_package,
            compression_class=compression_class,
            **kwargs
        )

    def extract_top_gradients(self):
        """
        Extract the indices and values of the topK gradients using Fisher information matrix.
        The gradients must have been accumulated.

        Returns
        -------
        tuple
            (a,b). a: The magnitudes of the topK gradients, b: Their indices.

        """

        # Use base class before the start round
        if self.iteration_number < self.start_round:
            if self.base is None:
                return super().extract_top_gradients()
            return self.base.extract_top_gradients()

        # Calculate the Fisher Matrix
        if self.iteration_number % self.frequency != 0 and (self.indices is not None):
            return None, self.indices
        dataloader = self.dataset.get_trainset()
        if self.subset_size is not None:
            indices = np.random.choice(len(dataloader.dataset), min(len(dataloader.dataset), self.subset_size))
            trainset = torch.utils.data.Subset(dataloader.dataset,indices)
            dataloader = torch.utils.data.DataLoader(
                                    dataset =  trainset,
                                    batch_size = 1,
                                    shuffle = False
                                )
        model_copy = self.model.__class__()
        model_copy.load_state_dict(self.model.state_dict())
        for param in model_copy.parameters():
            param.requires_grad_(True)
        F = FIM(model=model_copy,
        loader=dataloader,
        representation=PMatKFAC,
        n_output=self.number_of_classes)
        _, indices = torch.topk(F.get_diag(), round(self.alpha * F.get_diag().size(dim=0)), dim=0, sorted=False)
        self.indices = indices
        return None, indices
